﻿using OdeToFood.Data.Models;
using OdeToFood.Data.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OdeToFood.Web.Controllers
{
    public class RestaurantsController : Controller
    {
        private readonly IRestaurantData _db;

        public RestaurantsController(IRestaurantData db)
        {
            _db = db;
        }

        // GET: Restaurants
        public ActionResult Index()
        {
            var model = _db.GetAll();
            return View(model);
        }
        public ActionResult Details(int id)
        {
            var model = _db.GetRestaurantById(id);
            if(model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Restaurant restaurant)
        {
            //if (string.IsNullOrWhiteSpace(restaurant.Name))
            //{
            //    ModelState.AddModelError(nameof(restaurant.Name),"The name is required");
            //}
            if (ModelState.IsValid)
            {
                _db.Add(restaurant);
                return RedirectToAction(nameof(Details), new { Id = restaurant.Id });
            }
            return View();
        }
        public ActionResult Edit(int id)
        {
            Restaurant model = _db.GetRestaurantById(id);
            if (model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Restaurant restaurant)
        {
            if (ModelState.IsValid)
            {
                _db.Update(restaurant);
                TempData["Message"] = "You have saved the restaurant!";
                return RedirectToAction(nameof(Details), new { Id = restaurant.Id });
            }
            return View(restaurant);
        }
        public ActionResult Delete(int id)
        {
            Restaurant model = _db.GetRestaurantById(id);
            if(model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Restaurant restaurant)
        {
            _db.Delete(restaurant);
            return RedirectToAction(nameof(Index));
        }
    }
}