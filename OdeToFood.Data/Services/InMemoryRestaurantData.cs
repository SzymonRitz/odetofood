﻿using OdeToFood.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdeToFood.Data.Services
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        List<Restaurant> restaurants;

        public InMemoryRestaurantData()
        {
            this.restaurants = new List<Restaurant>()
            {
                new Restaurant(){ Id=1, Name="Scott's Pizza", Cusine = CusineType.Indian},
                new Restaurant(){ Id=2 ,Name="Tersiguels", Cusine = CusineType.French},
                new Restaurant(){ Id=3 ,Name="Mango Grove", Cusine = CusineType.Indian}
            };
        }

        public void Add(Restaurant restaurant)
        {
            restaurants.Add(restaurant);
            restaurant.Id = restaurants.Max(r => r.Id) + 1;
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return restaurants.OrderBy(r => r.Name);
        }

        public Restaurant GetRestaurantById(int id)
        {
            return restaurants.FirstOrDefault(r => r.Id == id);
        }

        public bool Update(Restaurant restaurant)
        {
            Restaurant existing = GetRestaurantById(restaurant.Id);
            if (existing != null)
            {
                existing.Name = restaurant.Name;
                existing.Cusine = restaurant.Cusine;
                return true;
            }
            return false;
        }
        public bool Delete(Restaurant restaurant)
        {
            if (restaurants.Contains(restaurant))
            {
                restaurants.Remove(restaurant);
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}
