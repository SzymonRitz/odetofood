﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using OdeToFood.Data.Models;

namespace OdeToFood.Data.Services
{
    public class SqlRestaurantData : IRestaurantData
    {
        private readonly OdeToFoodDbContext _db;

        public SqlRestaurantData(OdeToFoodDbContext db)
        {
            _db = db;
        }
        public void Add(Restaurant restaurant)
        {
            _db.Restaurants.Add(restaurant);
            _db.SaveChanges();
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _db.Restaurants;
        }

        public Restaurant GetRestaurantById(int id)
        {
            return _db.Restaurants.FirstOrDefault(r => r.Id == id);
        }

        public bool Update(Restaurant restaurant)
        {
            var entry = _db.Entry(restaurant);
            entry.State = EntityState.Modified;
            _db.SaveChanges();
            return true;
        }
        public bool Delete(Restaurant restaurant)
        {
            Restaurant toRemove = GetRestaurantById(restaurant.Id);
            _db.Restaurants.Remove(toRemove);
            _db.SaveChanges();
            return true;
        }
    }
}
